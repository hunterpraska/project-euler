# Problem 2
# Find the sum of all even terms of the Fibonacci sequence below 4 million

total = 0;
a = 1;
b = 1;

while (a < 4000000)
	if (a % 2 == 0)
		total += a;
	end
	a = a + b;
	b = a - b;
end
puts(total);