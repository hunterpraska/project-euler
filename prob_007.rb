# Problem 7
# What is the 10001 st prime number?

def is_prime(test)
	for i in 2..Math.sqrt(test)
		if (test % i == 0)
			return false;
		end
	end
	return true;
end

i = 0;
k = 1;
while i < 10001
	if is_prime(k)
		i += 1;
	end
	k += 1;
end

puts k;