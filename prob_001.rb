# Problem 1
# Find the sum of all the multiples of 3 or 5 below 1000

total = 0;
for i in 1..999
	if ((i % 3 == 0) || (i % 5 == 0))
		total += i;
	end
end
puts(total);