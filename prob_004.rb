# Problem 4
# Find the largest palindrome made from the product of two 3-digit numbers

k = 0;
for i in (999).downto(100)
	for j in (999).downto(100)
		mult = i * j;
		if  (mult > k) and (mult.to_s == mult.to_s.reverse)
			k = mult;
		end
	end
end

puts k;